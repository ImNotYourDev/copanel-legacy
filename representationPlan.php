<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
functions::isLoggedIn();
$teacher = functions::isTeacher(true);
$panelinfo = functions::getPanelInfo();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <?php
    if($teacher){
        echo "<title>Lehrer CP | Vertretungsplan</title>";
    }else{
        echo "<title>Schüler CP | Vertretungsplan</title>";
    }
    ?>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion p-0" style="background-color: #212529; transition: 1s;">
            <div class="container-fluid d-flex flex-column p-0">
                <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="dashboard.php">
                    <div class="sidebar-brand-icon"><i class="far fa-user"></i></div>
                    <?php
                    if($teacher){
                        echo "<div class='sidebar-brand-text mx-3'><span>Lehrer CP</span></div>";
                    }else{
                        echo "<div class='sidebar-brand-text mx-3'><span>Schüler CP</span></div>";
                    }
                    ?>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="nav navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" href="dashboard.php"><i class="fas fa-tachometer-alt"></i><span>Dashboard</span></a>
                        <a class="nav-link" href="tasks.php" style="color: rgba(255,255,255,0.8);"><i class="fas fa-tasks"></i><span>Aufgaben</span></a>
                        <a class="nav-link active" href="representationPlan.php"><i class="fas fa-table"></i><span>Vertretungsplan</span></a>
                        <a class='nav-link' href='exams.php'><i class='fas fa-sticky-note'></i><span>Prüfungen</span></a>
                        <a class='nav-link' href='group.php'><i class='fas fa-users'></i><span>Gruppe</span></a>
                        <?php
                        if($teacher){
                            echo "<hr />";
                            echo "<a class='nav-link' href='inbox.php'><i class='fas fa-newspaper'></i><span>Postfach</span></a>";
                            echo "<a class='nav-link' href='createTask.php'><i class='fas fa-plus'></i><span>Aufgaben erteilen</span></a>";
                            echo "<a class='nav-link' href='createdTasks.php'><i class='fas fa-list'></i><span>Erteilte Aufgaben</span></a>";
                            echo "<a class='nav-link' href='users.php'><i class='fas fa-users'></i><span>Benutzer</span></a>";
                            echo "<a class='nav-link' href='registerNewUser.php'><i class='fas fa-user-plus'></i><span>Benutzer hinzufügen</span></a>";
                            echo "<a class='nav-link' href='createGroup.php'><i class='fas fa-user-plus'></i><span>Gruppe hinzufügen</span></a>";
                        }
                        ?>
                        <hr />
                        <a class='nav-link' href='https://gitlab.com/ImNotYourDev/copanel-issue-tracker/-/issues'><i class='fas fa-bug'></i><span>Fehler melden</span></a>
                        <form method="post" action="php/post.php">
                            <button class="nav-link" name="button-logout" id="button-logout" type="submit" style="border: none; background-color: #232527"><i class='fas fa-sign-out-alt'></i><span>Ausloggen</span></button>
                        </form>
                    </li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <h3 class="text-dark mb-1">Vertretungsplan</h3>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#" style="font-size: 25px;"><i class="far fa-calendar-alt fa-fw"></i></a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-list dropdown-menu-right animated--grow-in"
                                role="menu">
                                <h6 class="dropdown-header">Tage</h6>
                                <a class="d-flex align-items-center dropdown-item" href="http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Montag.html">
                                    <div class="font-weight-bold">
                                        <div class="text-truncate"><span>Montag</span></div>
                                    </div>
                                </a>
                                <a class="d-flex align-items-center dropdown-item" href="http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Dienstag.html">
                                    <div class="font-weight-bold">
                                        <div class="text-truncate"><span>Dienstag</span></div>
                                    </div>
                                </a>
                                <a class="d-flex align-items-center dropdown-item" href="http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Mittwoch.html">
                                    <div class="font-weight-bold">
                                        <div class="text-truncate"><span>Mittwoch</span></div>
                                        <p class="small text-gray-500 mb-0"></p>
                                    </div>
                                </a>
                                <a class="d-flex align-items-center dropdown-item" href="http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Donnerstag.html">
                                    <div class="font-weight-bold">
                                        <div class="text-truncate"><span>Donnerstag</span></div>
                                    </div>
                                </a>
                                <a class="d-flex align-items-center dropdown-item" href="http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Freitag.html">
                                    <div class="font-weight-bold">
                                        <div class="text-truncate"><span>Freitag</span></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </nav>
                <p style="margin-left: 5px">Tag wählen und Vertretungsplan anschauen.</p>
                <div class="alert alert-warning" role="alert" style="margin: 20px"><span><strong>Vertretungsplan nicht sichtbar? Tag direkt wählen mit Kalernder Icon!</strong></span></div>
                <div>
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">Montag</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Dienstag</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-3">Mittwoch</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-4">Donnerstag</a></li>
                        <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-5">Freitag</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" style="margin: 20px" role="tabpanel" id="tab-1"><?php echo getPlan("montag")?></div>
                        <div class="tab-pane" style="margin: 20px" role="tabpanel" id="tab-2"><?php echo getPlan("dienstag")?></div>
                        <div class="tab-pane" style="margin: 20px" role="tabpanel" id="tab-3"><?php echo getPlan("mittwoch")?></div>
                        <div class="tab-pane" style="margin: 20px" role="tabpanel" id="tab-4"><?php echo getPlan("donnerstag")?></div>
                        <div class="tab-pane" style="margin: 20px" role="tabpanel" id="tab-5"><?php echo getPlan("freitag")?></div>
                    </div>
                </div>
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span><?php echo $panelinfo?> | Copyright © ImNotYourDev 2019-2020</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
    <script src="assets/js/theme.js"></script>
</body>
<?php
function getPlan(String $day): String
{
    if($day == "montag"){
        return utf8_encode(file_get_contents("http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Montag.html"));
    }
    if($day == "dienstag"){
        return utf8_encode(file_get_contents("http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Dienstag.html"));
    }
    if($day == "mittwoch"){
        return utf8_encode(file_get_contents("http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Mittwoch.html"));
    }
    if($day == "donnerstag"){
        return utf8_encode(file_get_contents("http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Donnerstag.html"));
    }
    if($day == "freitag"){
        return utf8_encode(file_get_contents("http://www.ms-kitzscher.de/Vertretungsplan/Vertretungsplan_Freitag.html"));
    }
    return null;
}

?>
</html>

<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
functions::isLoggedIn();
$teacher = functions::isTeacher();
$username = functions::getCurrentUserName();
$panelinfo = functions::getPanelInfo();
$groups = functions::getUsersGroups($username);
$groupids = implode(":", $groups);
if(!isset($_GET["groupids"])){
    functions::newLocation("group.php?groupids=$groupids");
    die();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <?php
    if($teacher){
        echo "<title>Lehrer CP | Gruppen</title>";
    }else{
        echo "<title>Schüler CP | Gruppen</title>";
    }
    ?>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
</head>

<body id="page-top">
<div id="wrapper">
    <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion p-0" style="transition: 1s;background-color: #212529;">
        <div class="container-fluid d-flex flex-column p-0">
            <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="dashboard.php">
                <div class="sidebar-brand-icon"><i class="far fa-user"></i></div>
                <?php
                if($teacher){
                    echo "<div class='sidebar-brand-text mx-3'><span>Lehrer CP</span></div>";
                }else{
                    echo "<div class='sidebar-brand-text mx-3'><span>Schüler CP</span></div>";
                }
                ?>
            </a>
            <hr class="sidebar-divider my-0">
            <ul class="nav navbar-nav text-light" id="accordionSidebar">
                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="dashboard.php"><i class="fas fa-tachometer-alt"></i><span>Dashboard</span></a>
                    <a class="nav-link" href="tasks.php" style="color: rgba(255,255,255,0.8);"><i class="fas fa-tasks"></i><span>Aufgaben</span></a>
                    <a class="nav-link" href="representationPlan.php"><i class="fas fa-table"></i><span>Vertretungsplan</span></a>
                    <a class='nav-link' href='exams.php'><i class='fas fa-sticky-note'></i><span>Prüfungen</span></a>
                    <a class='nav-link active' href='group.php'><i class='fas fa-users'></i><span>Gruppe</span></a>
                    <?php
                    if($teacher){
                        echo "<hr />";
                        echo "<a class='nav-link' href='inbox.php'><i class='fas fa-newspaper'></i><span>Postfach</span></a>";
                        echo "<a class='nav-link' href='createTask.php'><i class='fas fa-plus'></i><span>Aufgaben erteilen</span></a>";
                        echo "<a class='nav-link' href='createdTasks.php'><i class='fas fa-list'></i><span>Erteilte Aufgaben</span></a>";
                        echo "<a class='nav-link' href='users.php'><i class='fas fa-users'></i><span>Benutzer</span></a>";
                        echo "<a class='nav-link' href='registerNewUser.php'><i class='fas fa-user-plus'></i><span>Benutzer hinzufügen</span></a>";
                        echo "<a class='nav-link' href='createGroup.php'><i class='fas fa-user-plus'></i><span>Gruppe hinzufügen</span></a>";
                    }
                    ?>
                    <hr />
                    <a class='nav-link' href='https://gitlab.com/ImNotYourDev/copanel-issue-tracker/-/issues'><i class='fas fa-bug'></i><span>Fehler melden</span></a>
                    <form method="post" action="php/post.php">
                        <button class="nav-link" name="button-logout" id="button-logout" type="submit" style="border: none; background-color: #232527"><i class='fas fa-sign-out-alt'></i><span>Ausloggen</span></button>
                    </form>
                </li>
            </ul>
            <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
        </div>
    </nav>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                    <h3 class="text-dark">Gruppen</h3>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="card shadow">
                    <div class="card-header py-3" style="display: flex">
                        <?php
                        if($groups[0] == ""){
                            echo "<p class='text-primary font-weight-bold'>Du bist in keiner Gruppe!</p>";
                        }else{
                            echo "<p class='text-primary font-weight-bold'>Deine Gruppe(n):</p>";
                        }
                        ?>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                            <?php
                            if($groups[0] != ""){
                                require_once "php/table/groupsTable.php";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="bg-white sticky-footer">
            <div class="container my-auto">
                <div class="text-center my-auto copyright"><span><?php echo $panelinfo?> | Copyright © ImNotYourDev 2019-2020</span></div>
            </div>
        </footer>
    </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
<script src="assets/js/theme.js"></script>
</body>

</html>
<?php

/**
 *   _____ ____  _____                 _
 *  / ____/ __ \|  __ \               | |
 * | |   | |  | | |__) |_ _ _ __   ___| |
 * | |   | |  | |  ___/ _` | '_ \ / _ \ |
 * | |___| |__| | |  | (_| | | | |  __/ |
 *  \_____\____/|_|   \__,_|_| |_|\___|_| By ImNotYourDev
 *
 * COPanel, a webpanel for schools
 * Copyright (c) 2019-2020 ImNotYourDev
 *
 * Email: niebuhrsteve@gmail.com
 * Website: https://imnotyourdev.tk
 *
 * This software is distributed under "GNU General Public License v3.0".
 * This license allows you to use it and/or modify it but you are not at
 * all allowed to sell this webpanel at any cost. If found doing so the
 * necessary action required would be taken.
 *
 * COPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License v3.0 for more details.
 *
 * You should have received a copy of the GNU General Public License v3.0
 * along with this program. If not, see
 * <https://opensource.org/licenses/GPL-3.0>.
 * ------------------------------------------------------------------------
 */

/** used for base informations
 * DONT LOAD PATH FROM CONFIG TO IMPORT ANYTHING HERE!
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/COPanel.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/provider/MySqlAPI.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/provider/MySqlConnection.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/group/GroupManager.php";

/**
 * Class functions
 */
class functions
{
    /**
     * VARS
     */

    /** @var MySqlAPI $mysql */
    public static $mysql;

    /**
     * CONST
     */

    const METHOD_POST = 0;
    const METHOD_GET = 1;

    /**
     * COPanel - Informations
     */

    /**
     * @return String
     */
    public static function getPanelInfo(): String
    {
        return getPanelInformations();
    }

    /**
     * MySQL
     */

    /**
     * @return MySqlConnection|null
     */
    public static function getMySQL(): ?MySqlConnection
    {
        if(self::isConnected()){
            return self::$mysql::getSql(self::getDBName());
        }
        return self::getMySQL();
    }

    /**
     * @return bool
     * if not connected, connection get established
     */
    public static function isConnected(): bool
    {
        if(self::$mysql == null or count(self::$mysql::getConnections()) == 0){
            self::connect();
            return false;
        }
        return true;
    }

    /**
     * connect to db
     */
    public static function connect(): void
    {
        self::$mysql = new MySqlAPI();
        self::$mysql::createConnection(getPanelConfig()->get("address"), getPanelConfig()->get("username"), getPanelConfig()->get("password"), getPanelConfig()->get("db-name"), getPanelConfig()->get("port"));
    }

    /**
     * @return String
     */
    public static function getDBName(): String
    {
        return getPanelConfig()->get("db-name");
    }

    /**
     * POST - GET
     */

    /**
     * @param String $id
     * @param int $type
     * @return bool
     * @deprecated $_GET removeable -> no reason for get check here..
     * #dumpcode
     */
    public static function methodIdExists(String $id, int $type = self::METHOD_POST): bool
    {
        if($type == self::METHOD_POST){
            return isset($_POST[$id]);
        }
        return isset($_GET[$id]);
    }

    /**
     * @param int $type
     * @return array
     */
    public static function getMethodData($type = self::METHOD_POST): array
    {
        if($type == self::METHOD_POST){
            return $_POST;
        }
        return $_GET;
    }

    /**
     * @param String $key
     * @param int $type
     * @return String|null
     */
    public static function getMethodValue(String $key, int $type = self::METHOD_POST): ?String
    {
        if($type == self::METHOD_POST){
            return $_POST[$key];
        }
        return $_GET[$key];
    }

    /**
     * @param String $value
     * @param int $type
     * @return String|null
     */
    public static function getMethodKey(String $value, int $type = self::METHOD_POST): ?String
    {
        if($type == self::METHOD_POST){
            return array_keys($_POST, $value)[0];
        }
        return array_keys($_GET, $value)[0];
    }

    /**
     * @return String|null
     */
    public static function getEmbeddedKey(): ?String
    {
        $data = explode("_", implode("_", array_keys($_POST)));
        return $data[0];
    }

    /**
     * @return array|null
     */
    public static function getEmbeddedValues(): ?array
    {
        $data = explode("_", implode("_", array_keys($_POST)));
        return $data;
    }

    /**
     * @param String $fileName
     * @return String|null
     */
    public static function getFileBaseName(String $fileName): ?String
    {
        return $_FILES[$fileName]["name"];
    }

    /**
     * @param String $fileName
     * @return String|null
     */
    public static function getTempFileName(String $fileName): ?String
    {
        return $_FILES[$fileName]["tmp_name"];
    }

    /**
     * BROWSER
     */

    /**
     * @param String $location
     */
    public static function newLocation(String $location): void
    {
        header("Location: /$location");
    }

    /**
     * TASKS
     */

    /**
     * @return TaskManager
     */
    public static function getTaskManager(): TaskManager
    {
        return new TaskManager();
    }

    /**
     * GROUPS
     */

    /**
     * @return GroupManager
     */
    public static function getGroupManager(): GroupManager
    {
        return new GroupManager();
    }

    /**
     * STORAGE
     */

    /**
     * @return StorageManager
     */
    public static function getStorangeManager(): StorageManager
    {
        return new StorageManager();
    }

    /**
     * USER
     */

    /**
     * @param String $username
     * @param String $password
     * @param String|null $groupids
     * @param bool $teacher
     */
    public static function registerUser(String $username, String $password, ?String $groupids, ?bool $teacher): void
    {
        $newid = self::getNewUserId();
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);
        self::getMySQL()->query("INSERT INTO users(id, username, password, groups, teacher) VALUES('$newid', '$username', '$passwordHash', '$groupids', '$teacher')");
    }

    /**
     * @return int
     */
    public static function getNewUserId(): int
    {
        $result = mysqli_fetch_assoc(self::getMySQL()->query("SELECT MAX(id) FROM users"));
        return $result["MAX(id)"] + 1;
    }

    /**
     * @return array|null
     */
    public static function getCurrentUser(): ?array
    {
        $username = $_COOKIE["username"];
        $userdata = mysqli_fetch_assoc(self::getMySQL()->query("SELECT * FROM users WHERE username = '$username'"));
        $userdata["lasttime"] = time();
        return $userdata;
    }

    /**
     * @return String|null
     */
    public static function getCurrentUserName(): ?String
    {
        if(isset($_COOKIE["username"])){
            return $_COOKIE["username"];
        }
        return null;
    }

    /**
     * @param String $username
     * @param String $password
     * @return int
     */
    public static function handleLogin(String $username, String $password): int
    {
        if(password_verify($password, self::getPasswordHash($username))){
            self::setLoggedIn($username, $password);
            return 0;
        }else{
            return 1;
        }
    }

    /**
     * @param String $username
     * @return String
     */
    public static function getPasswordHash(String $username): String
    {
        $result = mysqli_fetch_assoc(self::getMySQL()->query("SELECT password FROM users WHERE username = '$username'"));
        $passwordhash = $result["password"];
        return $passwordhash;
    }

    /**
     * check for login and send user
     */
    public static function isLoggedIn()
    {
        if(isset($_COOKIE["username"]) and isset($_COOKIE["password"])){
            $username = $_COOKIE["username"];
            $password = $_COOKIE["password"];
            if(password_verify($password, self::getPasswordHash($username))){
                self::setLoggedIn($username, $password);
                return;
            }
        }
        self::setLoggedIn("", "", false);
        if(!isset($_GET["errid"])){
            self::newLocation("index.php?errid=2");
        }
    }

    /**
     * @param String $username
     * @param String|null $password
     * @param bool|null $login
     */
    public static function setLoggedIn(String $username = null, String $password = null, bool $login = true): void
    {
        $time = time();
        if($login){
            setcookie("username", $username, time() + 1800, "/");
            setcookie("password", $password, time() + 1800, "/");
            self::getMySQL()->query("UPDATE users SET lastseen = '$time' WHERE username = '$username'");
            return;
        }else{
            setcookie("username", "", time() - 1, "/");
            setcookie("password", "", time() - 1, "/");
            self::getMySQL()->query("UPDATE users SET lastseen = '$time' WHERE username = '$username'");
        }
    }

    /**
     * @return bool
     */
    public static function isNewUser(): bool
    {
        return self::getUserByName(functions::getCurrentUserName())["new"];
    }

    /**
     * @param String $password
     */
    public static function changePassword(String $password): void
    {
        $username = self::getCurrentUserName();
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);
        self::getMySQL()->query("UPDATE users SET password = '$passwordHash' WHERE username = '$username'");
    }

    /**
     * @param bool $new
     */
    public static function setNewUser(bool $new): void
    {
        $username = self::getCurrentUserName();
        self::getMySQL()->query("UPDATE users SET new = '$new' WHERE username = '$username'");
    }

    /**
     * @param bool $transport
     * @return bool|null
     */
    public static function isTeacher(bool $transport = false): bool
    {
        $isteacher = self::getCurrentUser()["teacher"];
        if($transport){
            if(!$isteacher){
                self::newLocation("dashboard.php");
            }
        }
        return $isteacher;
    }

    /**
     * @return array
     */
    public static function getAllUsers(): array
    {
        $return_data = [];
        foreach (self::getMySQL()->query("SELECT * FROM users") as $user){
            $return_data[] = $user;
        }
        return $return_data;
    }

    /**
     * @return array
     */
    public static function getOnlineUsers(): array
    {
        $return_data = [];
        foreach (self::getAllUsers() as $user){
            if($user["lastseen"] + 300 > time()){
                $return_data[] = $user;
            }
        }
        return $return_data;
    }

    /**
     * @param bool $completeOffline
     * @return array
     */
    public static function getOfflineUsers(bool $completeOffline = false): array
    {
        $return_data = [];
        if($completeOffline){
            foreach (self::getAllUsers() as $user){
                if($user["lastseen"] + 1800 < time()){
                    $return_data[] = $user;
                }
            }
        }else{
            foreach (self::getAllUsers() as $user){
                if($user["lastseen"] + 300 < time()){
                    $return_data[] = $user;
                }
            }
        }
        return $return_data;
    }

    /**
     * @return int
     */
    public static function countAllUsers(): int
    {
        return count(self::getAllUsers());
    }

    /**
     * @return int
     */
    public static function countOnlineUsers(): int
    {
        return count(self::getOnlineUsers());
    }

    /**
     * @return String
     */
    public static function getCompressedName(): String
    {
        return self::compressName(self::getCurrentUserName());
    }

    /**
     * @param String $username
     * @param int $groupid
     */
    public static function setGroup(String $username, int $groupid): void
    {
        $groups = self::getUsersGroups($username);
        $groups[] = $groupid;
        $groupids = implode(":", $groups);
        functions::getMySQL()->query("UPDATE users SET groups = '$groupids' WHERE username = '$username'");
    }

    /**
     * GROUP
     */

    /**
     * @param String $username
     * @return array|null
     */
    public static function getUsersGroups(String $username): array
    {
        $user = self::getUserByName($username);
        $ids = explode(":", $user["groups"]);
        $return_data = [];
        foreach ($ids as $id){
            $return_data[$id] = $id;
        }
        return $return_data;
    }

    /**
     * @param String $username
     * @param int $groupid
     * @return bool
     */
    public static function userHasGroup(String $username, int $groupid): bool
    {
        $usersgroups = self::getUsersGroups($username);
        if(isset($usersgroups[$groupid])){
            return true;
        }
        return false;
    }

    /**
     * UTILS
     * @param String $username
     * @return bool
     */

    public static function UsernameExists(String $username): bool
    {
        foreach (self::getAllUsers() as $user){
            if($user["username"] == $username){
                return true;
            }
        }
        return false;
    }

    /**
     * @param String $username
     * @return array
     */
    public static function getUserByName(String $username): array
    {
        $user = mysqli_fetch_assoc(functions::getMySQL()->query("SELECT * FROM users WHERE username = '$username'"));
        return $user;
    }

    /**
     * @param String $username
     * @return bool
     */
    public static function isUserTeacher(String $username): bool
    {
        return self::getUserByName($username)["teacher"];
    }

    /**
     * @param int $groupid
     * @return array
     */
    public static function getGroupsUsers(int $groupid): array
    {
        $return_data = [];
        foreach (self::getAllUsers() as $user){
            if(self::userHasGroup($user["username"], $groupid)){
                $return_data[] = $user;
            }
        }
        return $return_data;
    }

    /**
     * @param String $username
     * @return String
     */
    public static function compressName(String $username): String
    {
        $row = explode(" ", $username);
        return implode("#", $row);
    }

    /**
     * @param String $compressedName
     * @return String
     */
    public static function deCompressName(String $compressedName): String
    {
        $row = explode("#", $compressedName);
        return implode(" ", $row);
    }
}
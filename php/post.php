<?php

/**
 *   _____ ____  _____                 _
 *  / ____/ __ \|  __ \               | |
 * | |   | |  | | |__) |_ _ _ __   ___| |
 * | |   | |  | |  ___/ _` | '_ \ / _ \ |
 * | |___| |__| | |  | (_| | | | |  __/ |
 *  \_____\____/|_|   \__,_|_| |_|\___|_| By ImNotYourDev
 *
 * COPanel, a webpanel for schools
 * Copyright (c) 2019-2020 ImNotYourDev
 *
 * Email: niebuhrsteve@gmail.com
 * Website: https://imnotyourdev.tk
 *
 * This software is distributed under "GNU General Public License v3.0".
 * This license allows you to use it and/or modify it but you are not at
 * all allowed to sell this webpanel at any cost. If found doing so the
 * necessary action required would be taken.
 *
 * COPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License v3.0 for more details.
 *
 * You should have received a copy of the GNU General Public License v3.0
 * along with this program. If not, see
 * <https://opensource.org/licenses/GPL-3.0>.
 * ------------------------------------------------------------------------
 */

/**
 * functions files
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/TaskManager.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/Task.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/storage/StorageManager.php";

$post = $_POST;

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(functions::methodIdExists("button-register-asuser")){
        $username = functions::getMethodValue("input-username");
        $password = functions::getMethodValue("input-password");
        $password2 = functions::getMethodValue("input-password-2");
        $groups = "";

        if(functions::UsernameExists($username)){
            functions::newLocation("register.php?error=true&errid=0");
            die();
        }
        if($password != $password2){
            functions::newLocation("register.php?error=true&errid=1");
            die();
        }

        functions::registerUser($username, $password, $groups, false);
        functions::newLocation("index.php?error=true&errid=2");
    }

    if($_POST["method"] == "login"){
        echo functions::handleLogin(functions::getMethodValue("username"), functions::getMethodValue("password"));
        die();
    }

    if($_POST["method"] == "changepassword"){
        $password = functions::getMethodValue("password");
        if(password_verify($password, functions::getPasswordHash(functions::getCurrentUserName()))){
            echo 2;
            die();
        }
        functions::changePassword(functions::getMethodValue("password"));
        functions::setNewUser(false);
        echo 0;
        die();
    }

    if($_POST["method"] == "markAllAsRead"){
        $tasks = functions::getTaskManager()->loadAllUserTasks(Task::TASK_TYPE_NEW);
        /** @var Task $task */
        foreach($tasks as $task){
            $task->setType(Task::TASK_TYPE_PENDING);
            functions::getTaskManager()->updateTask($task);
        }
        echo 0;
        die();
    }

    if(functions::methodIdExists("button-logout")){
        functions::setLoggedIn("", "", false);
        functions::newLocation("index.php");
    }

    if(functions::methodIdExists("button-seen-inbox")){
        $tasks = functions::getTaskManager()->loadAllSubmittedTasksForTeacher(Task::TASK_TYPE_SUBMITTED_NEW);
        /** @var Task $task */
        foreach($tasks as $task){
            $task->setType(Task::TASK_TYPE_SUBMITTED_PENDING);
            functions::getTaskManager()->updateTask($task);
        }
        functions::newLocation("inbox.php");
    }

    if(functions::methodIdExists("button-createtask")){
        $taskname = functions::getMethodValue("input-createtask-taskname");
        $description = functions::getMethodValue("input-createtask-description");
        $creator = functions::getCurrentUserName();
        $groupnames = [];
        foreach (array_keys(functions::getMethodData()) as $key){
            $row = explode("_", $key);
            if($row[0] == "checkbox-usegroup"){
                $groupnames[] = $row[1];
            }
        }
        if(isset($_FILES["file-createtask"])){
            $fileId = "file-createtask";
            functions::getStorangeManager()->storeNewFile($fileId, functions::getCurrentUserName());
        }
        $allusers = [];;
        foreach ($groupnames as $groupname){
            $group = functions::getGroupManager()->getGroupByName($groupname);
            $allusers[] = functions::getGroupsUsers($group->getId());
        }
        foreach ($allusers[0] as $user){
            unset($allusers[$key]);
            if($user["username"] != $creator){
                $task = new Task();
                $task->setId(functions::getTaskManager()->getNewTaskId());
                $task->setUsername($user["username"]);
                $task->setTaskname($taskname);
                $task->setDescription($description);
                if(isset($_FILES["file-createtask"])){
                    $task->setFile(functions::getFileBaseName($fileId));
                }
                $task->setCreator($creator);
                $task->setCreationDate(date("d-m-Y", time()));
                $task->setLastEdited(date("d-m-Y", time()));
                $task->setType(Task::TASK_TYPE_NEW);
                functions::getTaskManager()->addTask($task);
            }
        }
        functions::newLocation("createdTasks.php");
    }

    if(functions::methodIdExists("button-creategroup")){
        $groupname = functions::getMethodValue("input-creategroup-groupname");
        $users = [];
        $methodData = functions::getMethodData();
        unset($methodData["button-creategroup"]);
        unset($methodData["input-creategroup-groupname"]);
        $keys = array_keys($methodData);
        foreach ($keys as $userkey){
            $row = explode("_", $userkey);
            $rowname = explode("#", $row[1]);
            $username = implode(" ", $rowname);
            $users[] = $username;
        }
        $group = new Group();
        $groupid = functions::getGroupManager()->getNewGroupId();
        $group->setId($groupid);
        $group->setGroupname($groupname);
        $group->setLeader(functions::getCurrentUserName());
        $group->setCreation(date("H:i d-m-Y"));

        if(!functions::getGroupManager()->groupExists($groupname)){
            functions::getGroupManager()->addGroup($group);
            foreach ($users as $user){
                functions::setGroup($user, $groupid);
            }
            functions::setGroup(functions::getCurrentUserName(), $groupid);
            functions::newLocation("group.php");
        }else{
            functions::newLocation("createGroup.php");
        }
    }

    if(functions::methodIdExists("button-register")){
        $newusername = functions::getMethodValue("input-register-username");
        $password = functions::getMethodValue("input-register-password");
        $teacherbox = functions::getMethodValue("checkbox-register-teacher");
        $groupnames = [];
        foreach (array_keys(functions::getMethodData()) as $key){
            $row = explode("_", $key);
            if($row[0] == "checkbox-usegroup"){
                $groupnames[] = $row[1];
            }
        }
        $groupids = [];
        foreach ($groupnames as $groupname){
            $group = functions::getGroupManager()->getGroupByName($groupname);
            $groupids[] = $group->getId();
        }
        $groups = implode(":", $groupids);

        functions::registerUser($newusername, $password, $groups, $teacher);
        functions::newLocation("users.php");
    }

    if(functions::getEmbeddedKey() == "button-seen"){
        $datastring = array_keys($_POST)[0];
        $data = explode("_", $datastring);
        $taskid = $data[1];

        $task = functions::getTaskManager()->getTask($taskid);
        $task->setLastEdited(date("d-m-Y", time()));
        $task->setType(Task::TASK_TYPE_PENDING);
        functions::getTaskManager()->updateTask($task);
        functions::newLocation("tasks.php");
    }

    if(functions::getEmbeddedKey() == "button-seen-submitted"){
        $datastring = array_keys($_POST)[0];
        $data = explode("_", $datastring);
        $taskid = $data[1];

        $task = functions::getTaskManager()->getTask($taskid);
        $task->setLastEdited(date("d-m-Y", time()));
        $task->setType(Task::TASK_TYPE_SUBMITTED_PENDING);
        functions::getTaskManager()->updateTask($task);
        functions::newLocation("inbox.php");
    }

    if(functions::getEmbeddedKey() == "button-done"){
        $datastring = array_keys($_POST)[0];
        $data = explode("_", $datastring);
        $taskid = $data[1];

        $task = functions::getTaskManager()->getTask($taskid);
        $task->setLastEdited(date("d-m-Y", time()));
        $task->setType(Task::TASK_TYPE_DONE);
        functions::getTaskManager()->updateTask($task);
        functions::newLocation("tasks.php");
    }

    if(functions::getEmbeddedKey() == "button-done-submitted"){
        $datastring = array_keys($_POST)[0];
        $data = explode("_", $datastring);
        $taskid = $data[1];

        $task = functions::getTaskManager()->getTask($taskid);
        $task->setLastEdited(date("d-m-Y", time()));
        $task->setType(Task::TASK_TYPE_SUBMITTED_DONE);
        functions::getTaskManager()->updateTask($task);
        functions::newLocation("inbox.php");
    }

    if(functions::getEmbeddedKey() == "button-submittask"){
        $datastring = array_keys($_POST)[0];
        $data = explode("_", $datastring);
        $username = functions::deCompressName($data[1]);
        $taskid = $data[2];
        $fileId = "input-file-submittask";

        functions::getStorangeManager()->storeNewFile($fileId, $username, StorageManager::FILE_DOWNLOAD_SUBMITTED);
        $task = functions::getTaskManager()->getTask($taskid);
        $task->setLastEdited(date("d-m-Y", time()));
        $task->setSubmitDate(date("d-m-Y", time()));
        $task->setType(Task::TASK_TYPE_SUBMITTED_NEW);
        $task->setSubmittedFile(functions::getFileBaseName($fileId));
        functions::getTaskManager()->updateTask($task);
        functions::newLocation("tasks.php");
    }
}
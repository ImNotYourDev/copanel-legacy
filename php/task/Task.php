<?php

/**
 *   _____ ____  _____                 _
 *  / ____/ __ \|  __ \               | |
 * | |   | |  | | |__) |_ _ _ __   ___| |
 * | |   | |  | |  ___/ _` | '_ \ / _ \ |
 * | |___| |__| | |  | (_| | | | |  __/ |
 *  \_____\____/|_|   \__,_|_| |_|\___|_| By ImNotYourDev
 *
 * COPanel, a webpanel for schools
 * Copyright (c) 2019-2020 ImNotYourDev
 *
 * Email: niebuhrsteve@gmail.com
 * Website: https://imnotyourdev.tk
 *
 * This software is distributed under "GNU General Public License v3.0".
 * This license allows you to use it and/or modify it but you are not at
 * all allowed to sell this webpanel at any cost. If found doing so the
 * necessary action required would be taken.
 *
 * COPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License v3.0 for more details.
 *
 * You should have received a copy of the GNU General Public License v3.0
 * along with this program. If not, see
 * <https://opensource.org/licenses/GPL-3.0>.
 * ------------------------------------------------------------------------
 */

class Task{
    /** @var int|null $id */
    public $id;
    /** @var String $username */
    public $username;
    /** @var String $taskname */
    public $taskname;
    /** @var String $description */
    public $description;
    /** @var String|null $file */
    public $file;
    /** @var String|null $submittedFile */
    public $submittedFile;
    /** @var String $creator */
    public $creator;
    /** @var String $lastEdited */
    public $lastEdited;
    /** @var String $creationDate */
    public $creationDate;
    /** @var String|null $submitDate */
    public $submitDate;
    /** @var int $type */
    public $type;

    const TASK_TYPE_NEW = 0;
    const TASK_TYPE_PENDING = 1;
    const TASK_TYPE_DONE = 2;
    const TASK_TYPE_SUBMITTED = 3;
    const TASK_TYPE_SUBMITTED_NEW = 4;
    const TASK_TYPE_SUBMITTED_PENDING = 5;
    const TASK_TYPE_SUBMITTED_DONE = 6;
    const TASK_USER = 7;
    const TASK_TEACHER = 8;
    const TASK_TYPE_ALL_SUBMITTED = 9;
    const TASK_TYPE_ALL = 10;


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return String
     */
    public function getUsername(): String
    {
        return $this->username;
    }

    /**
     * @param String $username
     */
    public function setUsername(String $username): void
    {
        $this->username = $username;
    }

    /**
     * @return String
     */
    public function getTaskname(): String
    {
        return $this->taskname;
    }

    /**
     * @param String $taskname
     */
    public function setTaskname(String $taskname): void
    {
        $this->taskname = $taskname;
    }

    /**
     * @return String
     */
    public function getDescription(): String
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription(String $description): void
    {
        $this->description = $description;
    }

    /**
     * @return String|null
     */
    public function getFile(): ?String
    {
        return $this->file;
    }

    /**
     * @param String|null $file
     */
    public function setFile(?String $file): void
    {
        $this->file = $file;
    }

    /**
     * @return String|null
     */
    public function getSubmittedFile(): ?String
    {
        return $this->submittedFile;
    }

    /**php/post.php
     * @param String|null $submittedFile
     */
    public function setSubmittedFile(?String $submittedFile): void
    {
        $this->submittedFile = $submittedFile;
    }

    /**
     * @return String
     */
    public function getCreator(): String
    {
        return $this->creator;
    }

    /**
     * @param String $creator
     */
    public function setCreator(String $creator): void
    {
        $this->creator = $creator;
    }

    /**
     * @return String
     */
    public function getLastEdited(): String
    {
        return $this->lastEdited;
    }

    /**
     * @param String $date
     */
    public function setLastEdited(String $date): void
    {
        $this->lastEdited = $date;
    }

    /**
     * @return String
     */
    public function getCreationDate(): String
    {
        return $this->creationDate;
    }

    /**
     * @param String $creationDate
     */
    public function setCreationDate(String $creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return String|null
     */
    public function getSubmitDate(): ?String
    {
        return $this->submitDate;
    }

    /**
     * @param String|null $submitDate
     */
    public function setSubmitDate(?String $submitDate): void
    {
        $this->submitDate = $submitDate;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }
}
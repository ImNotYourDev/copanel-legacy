<?php

/**
 *   _____ ____  _____                 _
 *  / ____/ __ \|  __ \               | |
 * | |   | |  | | |__) |_ _ _ __   ___| |
 * | |   | |  | |  ___/ _` | '_ \ / _ \ |
 * | |___| |__| | |  | (_| | | | |  __/ |
 *  \_____\____/|_|   \__,_|_| |_|\___|_| By ImNotYourDev
 *
 * COPanel, a webpanel for schools
 * Copyright (c) 2019-2020 ImNotYourDev
 *
 * Email: niebuhrsteve@gmail.com
 * Website: https://imnotyourdev.tk
 *
 * This software is distributed under "GNU General Public License v3.0".
 * This license allows you to use it and/or modify it but you are not at
 * all allowed to sell this webpanel at any cost. If found doing so the
 * necessary action required would be taken.
 *
 * COPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License v3.0 for more details.
 *
 * You should have received a copy of the GNU General Public License v3.0
 * along with this program. If not, see
 * <https://opensource.org/licenses/GPL-3.0>.
 * ------------------------------------------------------------------------
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/Task.php";

class TaskManager
{
    /**
     * @param int $type
     * @return array
     */
    public function loadAllUserTasks(int $type = Task::TASK_TYPE_NEW): array
    {
        $return_data = [];
        $username = functions::getCurrentUserName();
        if($type == Task::TASK_TYPE_ALL){
            foreach (functions::getMySQL()->query("SELECT * FROM tasks WHERE username = '$username'") as $taskdata){
                $task = $this->makeTaskClass($taskdata);
                $return_data[] = $task;
            }
        }elseif($type == Task::TASK_TYPE_ALL_SUBMITTED){
            foreach (functions::getMySQL()->query("SELECT * FROM tasks WHERE username = '$username' AND type > '2'") as $taskdata){
                $task = $this->makeTaskClass($taskdata);
                $return_data[] = $task;
            }
        }else{
            foreach (functions::getMySQL()->query("SELECT * FROM tasks WHERE username = '$username' AND type = '$type'") as $taskdata){
                $task = $this->makeTaskClass($taskdata);
                $return_data[] = $task;
            }
        }
        return $return_data;
    }

    /**
     * @param int $type
     * @return array
     */
    public function loadAllSubmittedTasksForTeacher(int $type = Task::TASK_TYPE_SUBMITTED_NEW): array
    {
        $return_data = [];
        $creator = functions::getCurrentUserName();
        if($type == Task::TASK_TYPE_ALL_SUBMITTED){
            foreach (functions::getMySQL()->query("SELECT * FROM tasks WHERE creator = '$creator' AND type > '2'") as $taskdata){
                $task = $this->makeTaskClass($taskdata);
                $return_data[] = $task;
            }
        }else{
            foreach (functions::getMySQL()->query("SELECT * FROM tasks WHERE creator = '$creator' AND type = '$type'") as $taskdata){
                $task = $this->makeTaskClass($taskdata);
                $return_data[] = $task;
            }
        }

        return $return_data;
    }

    /**
     * @param array $taskdata
     * @return Task
     */
    public function makeTaskClass(array $taskdata): Task
    {
        $task = new Task();
        $task->setId($taskdata["id"]);
        $task->setTaskname($taskdata["taskname"]);
        $task->setDescription($taskdata["description"]);
        $task->setUsername($taskdata["username"]);
        $task->setFile($taskdata["file"]);
        $task->setSubmittedFile($taskdata["submittedFile"]);
        $task->setCreator($taskdata["creator"]);
        $task->setLastEdited($taskdata["lastEdited"]);
        $task->setCreationDate($taskdata["creationDate"]);
        $task->setSubmitDate($taskdata["submitDate"]);
        $task->setType($taskdata["type"]);

        return $task;
    }

    /**
     * @param int $type
     * @param int $tasktype
     * @return int
     */
    public function countTasks(int $type = Task::TASK_USER, int $tasktype = Task::TASK_TYPE_NEW): int
    {
        if($type == Task::TASK_USER){
            return count($this->loadAllUserTasks($tasktype));
        }else{
            return count($this->loadAllSubmittedTasksForTeacher($tasktype));
        }
    }

    /**
     * @param int $id
     * @param String $taskname
     * @return array
     */
    public function loadTask(int $id): array
    {
        return mysqli_fetch_assoc(functions::getMySQL()->query("SELECT * FROM tasks WHERE id = '$id'"));
    }

    /**
     * @return array
     */
    public function loadAllTasks(): array
    {
        $return_data = [];
        foreach (functions::getMySQL()->query("SELECT * FROM tasks") as $task){
            $return_data[] = $this->makeTaskClass($task);
        }
        return $return_data;
    }

    /**
     * @param int $id
     * @param String $taskname
     * @return Task
     */
    public function getTask(int $id): Task
    {
        $result = $this->loadTask($id);
        $task = $this->makeTaskClass($result);

        return $task;
    }

    /**
     * @param Task $task
     */
    public function addTask(Task $task): void
    {
        $id = $task->getId();
        $username = $task->getUsername();
        $taskname = $task->getTaskname();
        $description = $task->getDescription();
        $file = $task->getFile();
        $submittedFile = $task->getSubmittedFile();
        $creator = $task->getCreator();
        $lastEdited = $task->getLastEdited();
        $creationDate = $task->getCreationDate();
        $submitDate = $task->getSubmitDate();
        $type = $task->getType();

        functions::getMySQL()->query("INSERT INTO tasks(id, username, taskname, description, file, submittedFile, creator, lastEdited, creationDate, submitDate, type) VALUES('$id', '$username', '$taskname', '$description', '$file', '$submittedFile', '$creator', '$lastEdited', '$creationDate', '$submitDate', '$type')");
    }

    /**
     * @param Task $task
     */
    public function removeTask(Task $task): void
    {
        $taskname = $task->getTaskname();
        $id = $task->getId();
        functions::getMySQL()->query("DELETE FROM tasks WHERE id = '$id' AND taskname = '$taskname'");
    }

    /**
     * @return int
     */
    public function getNewTaskId(): int
    {
        $result = mysqli_fetch_assoc(functions::getMySQL()->query("SELECT MAX(id) FROM tasks"));
        return $result["MAX(id)"] + 1;
    }

    /**
     * @param Task $task
     */
    public function updateTask(Task $task): void
    {
        $id = $task->getId();
        $username = $task->getUsername();
        $taskname = $task->getTaskname();
        $description = $task->getDescription();
        $file = $task->getFile();
        $submittedFile = $task->getSubmittedFile();
        $creator = $task->getCreator();
        $lastEdited = $task->getLastEdited();
        $creationDate = $task->getCreationDate();
        $submitDate = $task->getSubmitDate();
        $type = $task->getType();

        functions::getMySQL()->query("UPDATE tasks SET id = '$id', username = '$username', taskname = '$taskname', description = '$description', file = '$file', submittedFile = '$submittedFile', creator = '$creator', lastEdited = '$lastEdited', creationDate = '$creationDate', submitDate = '$submitDate', type = '$type' WHERE username = '$username' AND id = '$id' AND taskname = '$taskname'");
    }
}
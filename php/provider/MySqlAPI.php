<?php

/**
 *   _____ ____  _____                 _
 *  / ____/ __ \|  __ \               | |
 * | |   | |  | | |__) |_ _ _ __   ___| |
 * | |   | |  | |  ___/ _` | '_ \ / _ \ |
 * | |___| |__| | |  | (_| | | | |  __/ |
 *  \_____\____/|_|   \__,_|_| |_|\___|_| By ImNotYourDev
 *
 * COPanel, a webpanel for schools
 * Copyright (c) 2019-2020 ImNotYourDev
 *
 * Email: niebuhrsteve@gmail.com
 * Website: https://imnotyourdev.tk
 *
 * This software is distributed under "GNU General Public License v3.0".
 * This license allows you to use it and/or modify it but you are not at
 * all allowed to sell this webpanel at any cost. If found doing so the
 * necessary action required would be taken.
 *
 * COPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License v3.0 for more details.
 *
 * You should have received a copy of the GNU General Public License v3.0
 * along with this program. If not, see
 * <https://opensource.org/licenses/GPL-3.0>.
 * ------------------------------------------------------------------------
 */

/**
 * Class MySqlAPI
 */
class MySqlAPI
{
    /** @var array $connections */
    private static $connections = [];

    /**
     * @param string $ip
     * @param string $user_name
     * @param string $password
     * @param string $DB_Name
     * @param int $port
     */
    public static function createConnection(string $ip, string $user_name, string $password, string $DB_Name, int $port = 3306): void
    {
        if(self::getSql($DB_Name) == null) {
            self::$connections[$DB_Name] = new MySqlConnection($ip, $user_name, $password, $DB_Name, $port);
        }else{
            echo "Connection already established!";
        }
    }

    /**
     * @return array
     */
    public static function getConnections(): array
    {
        return self::$connections;
    }

    /**
     * @param string $DB_Name
     * @return MySqlConnection|null
     */
    public static function getSql(string $DB_Name): ?MySqlConnection
    {
        if(array_key_exists($DB_Name, self::getConnections())) {
            return self::$connections[$DB_Name];
        }
        return null;
    }

}
<?php

/**
 *   _____ ____  _____                 _
 *  / ____/ __ \|  __ \               | |
 * | |   | |  | | |__) |_ _ _ __   ___| |
 * | |   | |  | |  ___/ _` | '_ \ / _ \ |
 * | |___| |__| | |  | (_| | | | |  __/ |
 *  \_____\____/|_|   \__,_|_| |_|\___|_| By ImNotYourDev
 *
 * COPanel, a webpanel for schools
 * Copyright (c) 2019-2020 ImNotYourDev
 *
 * Email: niebuhrsteve@gmail.com
 * Website: https://imnotyourdev.tk
 *
 * This software is distributed under "GNU General Public License v3.0".
 * This license allows you to use it and/or modify it but you are not at
 * all allowed to sell this webpanel at any cost. If found doing so the
 * necessary action required would be taken.
 *
 * COPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License v3.0 for more details.
 *
 * You should have received a copy of the GNU General Public License v3.0
 * along with this program. If not, see
 * <https://opensource.org/licenses/GPL-3.0>.
 * ------------------------------------------------------------------------
 */

/**
 * Class MySqlConnection
 */
class MySqlConnection
{
    /**
     * @var String $ip
     * @var String $port
     * @var String $db_name
     * @var String $password
     * @var String $user_name
     */
    private $ip, $port, $db_name,$password, $user_name;
    /** @var $connection */
    private $connection;

    /**
     * MySqlConnection constructor.
     * @param string $ip
     * @param string $user_name
     * @param string $password
     * @param string $db_name
     * @param int $port
     */
    public function __construct(string $ip, string $user_name, string $password, string $db_name, int $port = 3306)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->db_name = $db_name;
        $this->user_name = $user_name;
        $this->password = $password;
        $this->createConnection();
    }

    /**
     * function createConnection
     */
    private function createConnection()
    {
        $this->connection = mysqli_connect($this->ip, $this->user_name, $this->password, $this->db_name, $this->port);
    }

    /**
     * @return String
     */
    public function getDataBaseName(): String
    {
        return $this->db_name;
    }

    /**
     * @return String
     */
    public function getUserName(): String
    {
        return $this->user_name;
    }

    /**
     * @return String
     */
    public function getIp(): String
    {
        return $this->ip;
    }

    /**
     * @return mixed
     */
    public function getConnection() : ?\mysqli
    {
        $this->connection->ping();
        return $this->connection;
    }

    /**
     * @param $sql_command
     * @return bool|mysqli_result
     */
    public function query($sql_command)
    {
        if(($con = $this->getConnection()->query($sql_command)) != false) {
            return $con;
        }else{
            $this->createConnection();
            return $this->getConnection()->query($sql_command);
        }
    }

}
<?php

/**
 *   _____ ____  _____                 _
 *  / ____/ __ \|  __ \               | |
 * | |   | |  | | |__) |_ _ _ __   ___| |
 * | |   | |  | |  ___/ _` | '_ \ / _ \ |
 * | |___| |__| | |  | (_| | | | |  __/ |
 *  \_____\____/|_|   \__,_|_| |_|\___|_| By ImNotYourDev
 *
 * COPanel, a webpanel for schools
 * Copyright (c) 2019-2020 ImNotYourDev
 *
 * Email: niebuhrsteve@gmail.com
 * Website: https://imnotyourdev.tk
 *
 * This software is distributed under "GNU General Public License v3.0".
 * This license allows you to use it and/or modify it but you are not at
 * all allowed to sell this webpanel at any cost. If found doing so the
 * necessary action required would be taken.
 *
 * COPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License v3.0 for more details.
 *
 * You should have received a copy of the GNU General Public License v3.0
 * along with this program. If not, see
 * <https://opensource.org/licenses/GPL-3.0>.
 * ------------------------------------------------------------------------
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/COPanel.php";

class StorageManager
{
    const FILE_DOWNLOAD_NEW_FILE = 0;
    const FILE_DOWNLOAD_SUBMITTED = 1;
    const FILE_DOWNLOAD_EXAMS = 2;

    /**
     * @return String
     */
    public function getDownloadDomain(): String
    {
        return getPanelConfig()->get("downloads");
    }

    /**
     * @param int $filetype
     * @return String
     */
    public function getDir(int $filetype = self::FILE_DOWNLOAD_NEW_FILE): String
    {
        switch ($filetype){
            case self::FILE_DOWNLOAD_NEW_FILE:
                return "/home/users/imnotyourdev/www/files/upload/task/";
                break;
            case self::FILE_DOWNLOAD_SUBMITTED:
                return "/home/users/imnotyourdev/www/files/upload/submitted/";
                break;
            case self::FILE_DOWNLOAD_EXAMS:
                return "/home/users/imnotyourdev/www/files/upload/exam/";
                break;
        }
        return null;
    }

    /**
     * @param int $filetype
     * @param String $fileowner
     * @param String $date
     * @param String $filename
     * @return String
     */
    public function getFile(int $filetype, ?String $fileowner, ?String $date, String $filename) : String
    {
        switch ($filetype){
            case self::FILE_DOWNLOAD_NEW_FILE:
                return $this->getDownloadDomain() . "task/$date/$fileowner/$filename";
                break;
            case self::FILE_DOWNLOAD_SUBMITTED:
                return $this->getDownloadDomain() . "submitted/$date/$fileowner/$filename";
                break;
            case self::FILE_DOWNLOAD_EXAMS:
                return $this->getDownloadDomain() . "exam/$filename";
                break;
        }
        return null;
    }

    /**
     * @return array|null
     */
    public function getAllExams(): ?array
    {
        return array_diff(scandir($this->getDir(self::FILE_DOWNLOAD_EXAMS)), array('..', '.'));
    }

    /**
     * @param String $file
     * @param String $fileowner
     * @param int $type
     */
    public function storeNewFile(String $fileId, String $fileowner, int $type = self::FILE_DOWNLOAD_NEW_FILE): void
    {
        $date = date("d-m-Y", time());
        $tmpName = functions::getTempFileName($fileId);
        $dir = $this->getDir($type) . $date . "/$fileowner/";
        $basename = functions::getFileBaseName($fileId);

        mkdir($dir, 0777, true);
        $targetFile = $dir . $basename;
        move_uploaded_file($tmpName, $targetFile);
    }
}
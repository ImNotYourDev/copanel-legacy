<table class="table dataTable my-0" id="dataTable">
    <thead>
    <tr>
        <th>Datei</th>
        <th>Aufgabe</th>
        <th>Beschreibung</th>
        <th>Für Benutzer</th>
        <th>Erstellt</th>
    </tr>
    </thead>
    <tbody>
    <style>
        thead{
            text-align: center;
        }

        tbody{
            vertical-align: middle;
            text-align: center;
        }
    </style>
    <?php
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/TaskManager.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/Task.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/storage/StorageManager.php";
    $username = functions::getCurrentUserName();
    $tasks = functions::getTaskManager()->loadAllTasks();
    /** @var Task $task */
    foreach ($tasks as $task){
        if($task->getCreator() == $username){
            echo "<tr>";
            if($task->getFile() == null or $task->getFile() == "NULL"){
                echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
            }else{
                $downloadFile = functions::getStorangeManager()->getFile(StorageManager::FILE_DOWNLOAD_NEW_FILE, $task->getCreator(), $task->getCreationDate(), $task->getFile());
                echo "<td><a href='$downloadFile'><button class='btn' id='button-download-tasks'><i class='fa fa-download'></i> Download</button></a></td>";
            }
            echo "<td>" . $task->getTaskname() . "</td>";
            echo "<td>" . $task->getDescription() . "</td>";
            echo "<td>" . $task->getUsername() . "</td>";
            echo "<td>" . $task->getCreationDate() . "</td>";
            echo "</tr>";
        }
    }
    ?>
    <script>
    </script>
    </tbody>
    <thead>
    <tr>
        <th>Datei</th>
        <th>Aufgabe</th>
        <th>Beschreibung</th>
        <th>Für Benutzer</th>
        <th>Erstellt</th>
    </tr>
    </thead>
</table>
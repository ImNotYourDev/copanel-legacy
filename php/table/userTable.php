<table class="table dataTable my-0" id="dataTable">
    <thead>
    <tr>
        <th>Online</th>
        <th>Benutzername</th>
        <th>Zuletzt Online</th>
        <th>Lehrer</th>
    </tr>
    </thead>
    <tbody>
    <style>
        thead{
            text-align: center;
        }

        tbody{
            vertical-align: middle;
            text-align: center;
        }

        .offline{
            border-left: .25rem solid #c4342d!important;
        }

        .online{
            border-left: .25rem solid #1cc88a!important;
        }

    </style>
    <?php
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
    $sort_users = [
        "online" => functions::getOnlineUsers(),
        "offline" => functions::getOfflineUsers()
    ];
    foreach($sort_users["online"] as $row) {
        $teacher = functions::isUserTeacher($row["username"]);
        echo "<tr class='online'>";
        echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-check-circle'></i></td>";
        echo "<td>" . $row['username'] . "</td>";
        echo "<td>Online</td>";
        if($teacher){
            echo "<td>Ja</td>";
        }else{
            echo "<td>Nein</td>";
        }
        echo "</tr>";
    }
    foreach($sort_users["offline"] as $row) {
        $teacher = functions::isUserTeacher($row["username"]);
        echo "<tr class='offline'>";
        echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
        echo "<td>" . $row['username'] . "</td>";
        echo "<td>" . date("H:i - d.m.Y", $row['lastseen']) . "</td>";
        if($teacher){
            echo "<td>Ja</td>";
        }else{
            echo "<td>Nein</td>";
        }
        echo "</tr>";
    }
    ?>
    <script>
    </script>
    </tbody>
    <thead>
    <tr>
        <th>Online</th>
        <th>Benutzername</th>
        <th>Zuletzt Online</th>
        <th>Lehrer</th>
    </tr>
    </thead>
</table>
<table class="table dataTable my-0" id="dataTable">
    <thead>
    <tr>
        <th>Fach</th>
        <th>Jahr</th>
        <th>Prüfung</th>
    </tr>
    </thead>
    <tbody>
    <style>
        thead{
            text-align: center;
        }

        tbody{
            vertical-align: middle;
            text-align: center;
        }

        #downloadtask{
            background-color: DodgerBlue;
            border: none;
            color: white;
            padding: 12px 30px;
            cursor: pointer;
            font-size: 20px;
        }
    </style>
    <?php
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/storage/StorageManager.php";
    $files = functions::getStorangeManager()->getAllExams();
    foreach ($files as $filename){
        echo "<tr>";
        $data = explode("_", $filename);
        $fach = $data[0];
        $jahr = $data[1];
        $dir = functions::getStorangeManager()->getDir(StorageManager::FILE_DOWNLOAD_EXAMS);
        $file = $dir . $filename;

        echo "<td>$fach</td>";
        echo "<td>$jahr</td>";
        echo "<td><a href=$file><button class='btn' id='downloadtask'><i class='fa fa-download'></i> Download</button></a></td>";
        echo "</tr>";
    }
    ?>
    <script>
    </script>
    </tbody>
    <thead>
    <tr>
        <th>Fach</th>
        <th>Jahr</th>
        <th>Prüfung</th>
    </tr>
    </thead>
</table>
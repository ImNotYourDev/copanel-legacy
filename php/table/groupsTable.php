<table class="table dataTable my-0" id="dataTable">
    <thead>
    <tr>
        <th>Gruppen Name</th>
        <th>Gruppen Ersteller</th>
        <th>Erstelt</th>
    </tr>
    </thead>
    <tbody>
    <style>
        thead{
            text-align: center;
        }

        tbody{
            vertical-align: middle;
            text-align: center;
        }
    </style>
    <?php
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
    $getdata = functions::getMethodValue("groupids", functions::METHOD_GET);
    $groupids = explode(":", $getdata);

    foreach ($groupids as $groupid){
        $group = functions::getGroupManager()->getGroup((int) $groupid);
        $groupname = $group->getGroupname();
        $groupleader = $group->getLeader();
        $groupdate = $group->getCreation();

        echo "<tr>";
        echo "<td>$groupname</td>";
        echo "<td>$groupleader</td>";
        echo "<td>$groupdate</td>";
        echo "</tr>";
    }
    ?>
    <script>
    </script>
    </tbody>
    <thead>
    <tr>
        <th>Gruppen Name</th>
        <th>Gruppen Ersteller</th>
        <th>Erstelt</th>
    </tr>
    </thead>
</table>
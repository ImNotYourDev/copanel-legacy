<table class="table dataTable my-0" id="dataTable">
    <thead>
    <tr>
        <th>Status</th>
        <th>Datei</th>
        <th>Aufgabe</th>
        <th>Beschreibung</th>
        <th>Sender</th>
        <th>Erhalten</th>
        <th>Gesehen</th>
        <th>Fertig</th>
    </tr>
    </thead>
    <tbody>
    <style>
        thead{
            text-align: center;
        }

        tbody{
            vertical-align: middle;
            text-align: center;
        }

        .new{
            border-left: .25rem solid #c4342d!important;
        }

        .seen{
            border-left: .25rem solid #f6c23e!important;
        }

        .done{
            border-left: .25rem solid #1cc88a!important;
        }

    </style>
    <?php
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/TaskManager.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/Task.php";
    require_once $_SERVER["DOCUMENT_ROOT"] . "/php/storage/StorageManager.php";
    $username = functions::getCurrentUserName();
    $sorted_tasks = [
        "new" => functions::getTaskManager()->loadAllSubmittedTasksForTeacher(Task::TASK_TYPE_SUBMITTED_NEW),
        "seen" => functions::getTaskManager()->loadAllSubmittedTasksForTeacher(Task::TASK_TYPE_SUBMITTED_PENDING),
        "done" => functions::getTaskManager()->loadAllSubmittedTasksForTeacher(Task::TASK_TYPE_SUBMITTED_DONE)
    ];
    /** @var Task $task */
    foreach($sorted_tasks["new"] as $task) {
        echo "<tr class='new'>";
        echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
        if($task->getSubmittedFile() == null or $task->getSubmittedFile() == "NULL"){
            echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
        }else{
            $downloadFile = functions::getStorangeManager()->getFile(StorageManager::FILE_DOWNLOAD_SUBMITTED, $task->getUsername(), $task->getSubmitDate(), $task->getSubmittedFile());
            echo "<td><a href='$downloadFile'><button class='btn' id='button-download-tasks'><i class='fa fa-download'></i> Download</button></a></td>";
        }
        echo "<td>" . $task->getTaskname() . "</td>";
        echo "<td>" . $task->getDescription() . "</td>";
        echo "<td>" . $task->getUsername() . "</td>";
        echo "<td>" . $task->getSubmitDate() . "</td>";
        $taskid = $task->getId();
        $name = "button-seen-submitted_" . $taskid;
        echo "<form method='post' action='/php/post.php'>";
        echo "<td style='vertical-align: middle;'><button style='border: none' name=$name id=$name type='submit'><i class='fas fa-eye' style='color: #c4342d'></i></button></td>";
        echo "</form>";
        $name = "button-done-submitted_" . $taskid;
        echo "<form method='post' action='/php/post.php'>";
        echo "<td style='vertical-align: middle;'><button style='border: none' name=$name id=$name type='submit'><i class='fas fa-check-circle' style='color: #c4342d'></i></button></td>";
        echo "</form>";
        echo "</tr>";
    }
    foreach($sorted_tasks["seen"] as $task) {
        echo "<tr class='seen'>";
        echo "<td style='vertical-align: middle; color: #f6c23e'><i class='fas fa-minus-circle'></i></td>";
        if($task->getSubmittedFile() == null or $task->getSubmittedFile() == "NULL"){
            echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
        }else{
            $downloadFile = functions::getStorangeManager()->getFile(StorageManager::FILE_DOWNLOAD_SUBMITTED, $task->getUsername(), $task->getSubmitDate(), $task->getSubmittedFile());
            echo "<td><a href='$downloadFile'><button class='btn' id='button-download-tasks'><i class='fa fa-download'></i> Download</button></a></td>";
        }
        echo "<td>" . $task->getTaskname() . "</td>";
        echo "<td>" . $task->getDescription() . "</td>";
        echo "<td>" . $task->getUsername() . "</td>";
        echo "<td>" . $task->getSubmitDate() . "</td>";
        echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-eye'></i></td>";
        $taskid = $task->getId();
        $name = "button-done-submitted_" . $taskid;
        echo "<form method='post' action='/php/post.php'>";
        echo "<td style='vertical-align: middle;'><button style='border: none' name=$name id=$name type='submit'><i class='fas fa-check-circle' style='color: #c4342d'></i></button></td>";
        echo "</form>";
        echo "</tr>";
    }
    foreach($sorted_tasks["done"] as $task) {
        echo "<tr class='done'>";
        echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-check-circle'></i></td>";
        if($task->getSubmittedFile() == null or $task->getSubmittedFile() == "NULL"){
            echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
        }else{
            $downloadFile = functions::getStorangeManager()->getFile(StorageManager::FILE_DOWNLOAD_SUBMITTED, $task->getUsername(), $task->getSubmitDate(), $task->getSubmittedFile());
            echo "<td><a href='$downloadFile'><button class='btn' id='button-download-tasks'><i class='fa fa-download'></i> Download</button></a></td>";
        }
        echo "<td>" . $task->getTaskname() . "</td>";
        echo "<td>" . $task->getDescription() . "</td>";
        echo "<td>" . $task->getUsername() . "</td>";
        echo "<td>" . $task->getSubmitDate() . "</td>";
        echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-eye'></i></td>";
        echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-check-circle'></i></td>";
        echo "</tr>";
    }
    ?>
    <script>
    </script>
    </tbody>
    <thead>
    <tr>
        <th>Status</th>
        <th>Datei</th>
        <th>Aufgabe</th>
        <th>Beschreibung</th>
        <th>Sender</th>
        <th>Erhalten</th>
        <th>Gesehen</th>
        <th>Fertig</th>
    </tr>
    </thead>
</table>
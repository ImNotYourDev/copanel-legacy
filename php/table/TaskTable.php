<table class="table dataTable my-0" id="dataTable">
    <thead>
    <tr>
        <th>Status</th>
        <th>Datei</th>
        <th>Aufgabe</th>
        <th>Beschreibung</th>
        <th>Ersteller</th>
        <th>Erhalten</th>
        <th>Gesehen</th>
        <th>Fertig</th>
        <th>Abgegeben</th>
    </tr>
    </thead>
    <tbody>
    <style>
        thead{
            text-align: center;
        }

        tbody{
            vertical-align: middle;
            text-align: center;
        }

        .new{
            border-left: .25rem solid #c4342d!important;
        }

        .seen{
            border-left: .25rem solid #f6c23e!important;
        }

        .done{
            border-left: .25rem solid #1cc88a!important;
        }

        .submitted{
            border-left: .25rem solid #e2b007!important;
        }

    </style>
        <?php
        require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
        require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/TaskManager.php";
        require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/Task.php";
        require_once $_SERVER["DOCUMENT_ROOT"] . "/php/storage/StorageManager.php";
        $sorted_tasks = [
            "new" => functions::getTaskManager()->loadAllUserTasks(Task::TASK_TYPE_NEW),
            "seen" => functions::getTaskManager()->loadAllUserTasks(Task::TASK_TYPE_PENDING),
            "done" => functions::getTaskManager()->loadAllUserTasks(Task::TASK_TYPE_DONE),
            "submitted" => functions::getTaskManager()->loadAllUserTasks(Task::TASK_TYPE_ALL_SUBMITTED)
        ];
        /** @var Task $task */
        foreach($sorted_tasks["new"] as $task) {
            echo "<tr class='new'>";
            echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
            if($task->getFile() == null or $task->getFile() == "NULL"){
                echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
            }else{
                $downloadFile = functions::getStorangeManager()->getFile(StorageManager::FILE_DOWNLOAD_NEW_FILE, $task->getCreator(), $task->getCreationDate(), $task->getFile());
                echo "<td><a href='$downloadFile'><button class='btn' id='button-download-tasks'><i class='fa fa-download'></i> Download</button></a></td>";
            }
            echo "<td style='vertical-align: middle;'>" . $task->getTaskname() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getDescription() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getCreator() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getCreationDate() . "</td>";
            $taskid = $task->getId();
            $name = "button-seen_" . $taskid;
            echo "<form method='post' action='/php/post.php'>";
            echo "<td style='vertical-align: middle;'><button style='border: none' name=$name id=$name type='submit'><i class='fas fa-eye' style='color: #c4342d'></i></button></td>";
            echo "</form>";
            $name = "button-done_" . $taskid;
            echo "<form method='post' action='/php/post.php'>";
            echo "<td style='vertical-align: middle;'><button style='border: none' name=$name id=$name type='submit'><i class='fas fa-check-circle' style='color: #c4342d'></i></button></td>";
            echo "</form>";
            echo "<td style='vertical-align: middle;'><a class='fas fa-share-square' style='color: #c4342d'></a></td>";
            echo "</tr>";
        }
        foreach($sorted_tasks["seen"] as $task) {
            echo "<tr class='seen'>";
            echo "<td style='vertical-align: middle; color: #f6c23e'><i class='fas fa-minus-circle'></i></td>";
            if($task->getFile() == null or $task->getFile() == "NULL"){
                echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
            }else{
                $downloadFile = functions::getStorangeManager()->getFile(StorageManager::FILE_DOWNLOAD_NEW_FILE, $task->getCreator(), $task->getCreationDate(), $task->getFile());
                echo "<td><a href='$downloadFile'><button class='btn' id='button-download-tasks'><i class='fa fa-download'></i> Download</button></a></td>";
            }
            echo "<td style='vertical-align: middle;'>" . $task->getTaskname() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getDescription() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getCreator() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getCreationDate() . "</td>";
            echo "<td style='vertical-align: middle;'><i class='fas fa-eye' style='color: #1cc88a'></i></td>";
            $taskid = $task->getId();
            $name = "button-done_" . $taskid;
            echo "<form method='post' action='/php/post.php'>";
            echo "<td style='vertical-align: middle;'><button style='border: none' name=$name id=$name type='submit'><i class='fas fa-check-circle' style='color: #c4342d'></i></button></td>";
            echo "</form>";
            echo "<td style='vertical-align: middle;'><a class='fas fa-share-square' style='color: #c4342d'></a></td>";
            echo "</tr>";
        }
        foreach($sorted_tasks["done"] as $task) {
            echo "<tr class='done'>";
            echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-check-circle'></i></td>";
            if($task->getFile() == null or $task->getFile() == "NULL"){
                echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
            }else{
                $downloadFile = functions::getStorangeManager()->getFile(StorageManager::FILE_DOWNLOAD_NEW_FILE, $task->getCreator(), $task->getCreationDate(), $task->getFile());
                echo "<td><a href='$downloadFile'><button class='btn' id='button-download-tasks'><i class='fa fa-download'></i> Download</button></a></td>";
            }
            echo "<td style='vertical-align: middle;'>" . $task->getTaskname() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getDescription() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getCreator() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getCreationDate() . "</td>";
            echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-eye'></i></td>";
            echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-check-circle'></i></td>";
            $taskid = $task->getId();
            $redirection = "/submitTask.php?taskid=$taskid";
            echo "<td style='vertical-align: middle;'><a class='fas fa-share-square' style='color: #c4342d' href=$redirection></a></td>";
            echo "</tr>";
        }
        foreach($sorted_tasks["submitted"] as $task) {
            echo "<tr class='submitted'>";
            echo "<td style='vertical-align: middle; color: #e2b007'><i class='fas fa-star'></i></td>";
            if($task->getFile() == null or $task->getFile() == "NULL"){
                echo "<td style='vertical-align: middle; color: #c4342d'><i class='fas fa-times-circle'></i></td>";
            }else{
                $downloadFile = functions::getStorangeManager()->getFile(StorageManager::FILE_DOWNLOAD_NEW_FILE, $task->getCreator(), $task->getCreationDate(), $task->getFile());
                echo "<td><a href='$downloadFile'><button class='btn' id='button-download-tasks'><i class='fa fa-download'></i> Download</button></a></td>";
            }
            echo "<td style='vertical-align: middle;'>" . $task->getTaskname() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getDescription() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getCreator() . "</td>";
            echo "<td style='vertical-align: middle;'>" . $task->getCreationDate() . "</td>";
            echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-eye'></i></td>";
            echo "<td style='vertical-align: middle; color: #1cc88a'><i class='fas fa-check-circle'></i></td>";
            echo "<td style='vertical-align: middle;'><a class='fas fa-share-square' style='color: #1cc88a'></a></td>";
            echo "</tr>";
        }
        ?>
    <script>
    </script>
    </tbody>
    <thead>
    <tr>
        <th>Status</th>
        <th>Datei</th>
        <th>Aufgabe</th>
        <th>Beschreibung</th>
        <th>Ersteller</th>
        <th>Erhalten</th>
        <th>Gesehen</th>
        <th>Fertig</th>
        <th>Abgegeben</th>
    </tr>
    </thead>
</table>
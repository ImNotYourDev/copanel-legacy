<?php

/**
 *   _____ ____  _____                 _
 *  / ____/ __ \|  __ \               | |
 * | |   | |  | | |__) |_ _ _ __   ___| |
 * | |   | |  | |  ___/ _` | '_ \ / _ \ |
 * | |___| |__| | |  | (_| | | | |  __/ |
 *  \_____\____/|_|   \__,_|_| |_|\___|_| By ImNotYourDev
 *
 * COPanel, a webpanel for schools
 * Copyright (c) 2019-2020 ImNotYourDev
 *
 * Email: niebuhrsteve@gmail.com
 * Website: https://imnotyourdev.tk
 *
 * This software is distributed under "GNU General Public License v3.0".
 * This license allows you to use it and/or modify it but you are not at
 * all allowed to sell this webpanel at any cost. If found doing so the
 * necessary action required would be taken.
 *
 * COPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License v3.0 for more details.
 *
 * You should have received a copy of the GNU General Public License v3.0
 * along with this program. If not, see
 * <https://opensource.org/licenses/GPL-3.0>.
 * ------------------------------------------------------------------------
 */

class Group
{
    /** @var int|null $id */
    public $id;
    /** @var String $groupname */
    public $groupname;
    /** @var String $leader */
    public $leader;
    /** @var String $creation */
    public $creation;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return String
     */
    public function getGroupname(): String
    {
        return $this->groupname;
    }

    /**
     * @param String $groupname
     */
    public function setGroupname(String $groupname): void
    {
        $this->groupname = $groupname;
    }

    /**
     * @return String
     */
    public function getLeader(): String
    {
        return $this->leader;
    }

    /**
     * @param String $leader
     */
    public function setLeader(String $leader): void
    {
        $this->leader = $leader;
    }

    /**
     * @return String
     */
    public function getCreation(): String
    {
        return $this->creation;
    }

    /**
     * @param String $creation
     */
    public function setCreation(String $creation): void
    {
        $this->creation = $creation;
    }
}
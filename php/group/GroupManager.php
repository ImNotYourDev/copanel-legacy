<?php

/**
 *   _____ ____  _____                 _
 *  / ____/ __ \|  __ \               | |
 * | |   | |  | | |__) |_ _ _ __   ___| |
 * | |   | |  | |  ___/ _` | '_ \ / _ \ |
 * | |___| |__| | |  | (_| | | | |  __/ |
 *  \_____\____/|_|   \__,_|_| |_|\___|_| By ImNotYourDev
 *
 * COPanel, a webpanel for schools
 * Copyright (c) 2019-2020 ImNotYourDev
 *
 * Email: niebuhrsteve@gmail.com
 * Website: https://imnotyourdev.tk
 *
 * This software is distributed under "GNU General Public License v3.0".
 * This license allows you to use it and/or modify it but you are not at
 * all allowed to sell this webpanel at any cost. If found doing so the
 * necessary action required would be taken.
 *
 * COPanel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License v3.0 for more details.
 *
 * You should have received a copy of the GNU General Public License v3.0
 * along with this program. If not, see
 * <https://opensource.org/licenses/GPL-3.0>.
 * ------------------------------------------------------------------------
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/php/group/Group.php";

class GroupManager
{
    /**
     * @return array
     */
    public function getGroups(): array
    {
        $return_data = [];
        foreach (functions::getMySQL()->query("SELECT * FROM groups") as $groupdata){
            $return_data[] = $this->makeGroup($groupdata);
        }
        return $return_data;
    }

    /**
     * @param int $groupid
     * @return Group
     */
    public function getGroup(int $groupid): Group
    {
        $result = mysqli_fetch_assoc(functions::getMySQL()->query("SELECT * FROM groups WHERE id = '$groupid'"));
        $group = $this->makeGroup($result);

        return $group;
    }

    /**
     * @param Group $group
     */
    public function addGroup(Group $group): void
    {
        $id = $group->getId();
        $groupname = $group->getGroupname();
        $leader = $group->getLeader();
        $creation = $group->getCreation();
        functions::getMySQL()->query("INSERT INTO groups(id, groupname, leader, creation) VALUES('$id', '$groupname', '$leader', '$creation')");
    }

    /**
     * @param array $groupdata
     * @return Group
     */
    public function makeGroup(array $groupdata): Group
    {
        $group = new Group();
        $group->setId($groupdata["id"]);
        $group->setGroupname($groupdata["groupname"]);
        $group->setLeader($groupdata["leader"]);
        $group->setCreation($groupdata["creation"]);

        return $group;
    }

    /**
     * @param Group $group
     */
    public function updateGroup(Group $group): void
    {
        $id = $group->getId();
        $groupname = $group->getGroupname();
        $leader = $group->getLeader();
        $creation = $group->getCreation();
        functions::getMySQL()->query("UPDATE groups SET id = '$id', groupname = '$groupname', leader = '$leader', creation = '$creation' WHERE id = '$id'");
    }

    /**
     * @return int
     */
    public function getNewGroupId(): int
    {
        $result = mysqli_fetch_assoc(functions::getMySQL()->query("SELECT MAX(id) FROM groups"));
        return $result["MAX(id)"] + 1;
    }

    /**
     * @param String $groupname
     * @return Group
     */
    public function getGroupByName(String $groupname): Group
    {
        $result = mysqli_fetch_assoc(functions::getMySQL()->query("SELECT * FROM groups WHERE groupname = '$groupname'"));
        return $this->makeGroup($result);
    }

    /**
     * @param String $groupname
     * @return bool
     */
    public function groupExists(String $groupname): bool
    {
        /** @var Group $group */
        foreach ($this->getGroups() as $group){
            if($group->getGroupname() == $groupname){
                return true;
            }
        }
        return false;
    }
}
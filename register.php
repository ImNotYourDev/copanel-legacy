<?php
/**
 * required for some needed functions
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";

$panelinfo = functions::getPanelInfo();
$error_data = [
    "error" => null,
    "errid" => null
];
if(isset($_GET["error"])){
    $error_data["error"] = $_GET["error"];
    $error_data["errid"] = $_GET["errid"];
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>CP | Registrieren</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="assets/css/Login-screen.css">
    <link rel="stylesheet" href="assets/css/Main.css">
</head>
<body>
<div id="login-one" class="login-one">
    <form class="login-one-form" action="php/post.php" method="post">
        <div class="col">
            <div class="login-one-ico"><i class="fas fa-user-plus" id="lockico"></i></div>
            <div class="form-group">
                <div>
                    <h3 id="heading">Registrieren</h3>
                    <?php
                    if($error_data["error"] != null){
                        $errorid = $error_data["errid"];
                        if($errorid == 0){
                            echo "<div class='alert alert-warning' style='margin-left: 20px; margin-right: 20px' role='alert'><span><strong>Benutzername bereits vergeben!</strong></span></div>";
                        }
                        if($errorid == 1){
                            echo "<div class='alert alert-warning' style='margin-left: 20px; margin-right: 20px' role='alert'><span><strong>Passwort stimmt nicht überein!</strong></span></div>";
                        }
                    }
                    ?>
                </div>
                <input class="form-control" type="text" id="input-username" name="input-username" placeholder="Benutzername" minlength="4" maxlength="20">
                <input class="form-control" type="password" id="input-password" name="input-password" placeholder="Passwort" minlength="4" maxlength="20" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}">
                <input class="form-control" type="password" id="input-password-2" name="input-password-2" placeholder="Passwort erneut eingeben" minlength="4" maxlength="20" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}">
                <button class="btn btn-primary" id="button-register-asuser" name="button-register-asuser" type="submit">Registrieren</button>
                <p style="margin-top: 20px">Du hast schon ein Konto? <a href="index.php">Anmelden</a></p>
            </div>
        </div>
    </form>
</div>
<footer class="bg-white sticky-footer">
    <div class="container my-auto">
        <div class="text-center my-auto copyright"><span><?php echo $panelinfo?> | Copyright © ImNotYourDev 2019-2020</span></div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

//GET
var getdata = window.location.search.substr(1).split("&");
var $_GET = {};
for (var i = 0; i < getdata.length; i++) {
    var temp = getdata[i].split("=");
    $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
}

//login
function handleLogin() {
    document.getElementById("error-wait").style.display = "none";
    document.getElementById("error-pass").style.display = "none";
    document.getElementById("error-logout").style.display = "none";
    document.getElementById("error-new").style.display = "none";
    document.getElementById("error-full").style.display = "none";

    var name = document.getElementById("input-username").value;
    var pass = document.getElementById("input-password").value;
    var method = "login";

    document.getElementById("error-wait").style.display = null;

    $.ajax({
        type: "post",
        url: "php/post.php",
        data: {method:method,username:name,password:pass},
        success: function (response) {
            if(response != null) {
                if(response == 0){
                    window.location = "dashboard.php";
                }else{
                    document.getElementById("error-wait").style.display = "none";
                }
                if(response == 1) {
                    document.getElementById("error-pass").style.display = null;
                }
                if(response == 2) {
                    document.getElementById("error-logout").style.display = null;
                }
                if(response == 3) {
                    document.getElementById("error-new").style.display = null;
                }
                if(response == 4) {
                    document.getElementById("error-full").style.display = null;
                }
            }else {
                if($_GET != null){
                    if($_GET["errid"] == 2){
                        document.getElementById("error-logout").style.display = null;
                    }
                    if($_GET["errid"] == 3){
                        document.getElementById("error-new").style.display = null;
                    }
                    if($_GET["errid"] == 4){
                        document.getElementById("error-full").style.display = null;
                    }
                }
            }
        }
    });
}

$("#form-login").submit(function (e) {
    e.preventDefault();
});

//changepassword
function changepassword() {
    document.getElementById("error-same").style.display = "none";
    document.getElementById("error-empty").style.display = "none";
    document.getElementById("error-full").style.display = "none";
    document.getElementById("lockico").classList.remove("rotating");

    var pass = document.getElementById("input-password").value;
    var method = "changepassword";

    if(pass != ""){
        $.ajax({
            type: "post",
            url: "php/post.php",
            data: {method:method,password:pass},
            success: function (response) {
                if(response != null) {
                    if(response == 0) {
                        document.getElementById("lockico").classList.add("rotating");
                        document.getElementById("error-success").style.display = null;
                        window.location = "dashboard.php";
                    }
                    if(response == 2) {
                        document.getElementById("error-same").style.display = null;
                    }
                }else{
                    document.getElementById("error-full").style.display = null;
                }
            }
        });
    }else{
        document.getElementById("error-empty").style.display = null;
    }
}

function markAllAsread(site) {
    var method = "markAllAsRead";
    $.ajax({
        type: "post",
        url: "php/post.php",
        data: {method:method,site:site},
        success: function (response) {
            if(response == 0){
                if(site == "dashboard"){
                    var taskdata = "";

                    method = "getdata";
                    var data = "taskcount";
                    $.ajax({
                        type: "post",
                        url: "php/post.php",
                        data: {method:method,data:data},
                        success: function (response) {
                            taskdata = response;
                        }
                    });

                    var taskdatarow = taskdata.split(";");
                    var newtaskcount = taskdatarow.slice("0", taskdatarow.indexOf(","));
                    document.getElementById("new-task-count").innerHTML = ""
                }
            }else{
                window.location = "index.php?errid=4";
            }
        }
    });
}

$("#form-changepassword").submit(function (e) {
    e.preventDefault();
});

//other needed functions
window.onload = function () {
    if($_GET != null){
        if($_GET["errid"] == 2){
            document.getElementById("error-logout").style.display = null;
        }
        if($_GET["errid"] == 3){
            document.getElementById("error-new").style.display = null;
        }
        if($_GET["errid"] == 4){
            document.getElementById("error-full").style.display = null;
        }
    }
};

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
        end = new Date().getTime();
    }
}
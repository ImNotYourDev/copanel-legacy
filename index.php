<?php
/**
 * required for some needed functions
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
/**
 * check for login
 */
functions::isLoggedIn();
$panelinfo = functions::getPanelInfo();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>CP | Login</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="assets/css/Login-screen.css">
    <link rel="stylesheet" href="assets/css/Main.css">
</head>
<body>
    <div id="login-one" class="login-one">
        <form class="login-one-form" id="form-login">
            <div class="col">
                <div class="login-one-ico"><i class="fas fa-lock" id="lockico"></i></div>
                <div class="form-group">
                    <div>
                        <h3 id="heading">Login</h3>
                        <div id="error-wait" class='alert alert-success' style='margin: 20px; padding-bottom: 75px; display: none; transition: 2s' role='alert'>
                            <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                            <span><strong>Bitte warten...</strong></span>
                        </div>
                        <div class='alert alert-danger' id="error-pass" style='margin: 20px; display: none; transition: 2s' role='alert'><span><strong>Falsches Passwort!</strong></span></div>
                        <div class='alert alert-warning' id="error-logout" style='margin: 20px; display: none; transition: 2s' role='alert'><span><strong>Du wurdest Automatisch abgemeldet!</strong></span></div>
                        <div class='alert alert-success' id="error-new" style='margin: 20px; display: none; transition: 2s' role='alert'><span><strong>Konto erstellt! Anmelden</strong></span></div>
                        <div class='alert alert-danger' id="error-full" style='margin: 20px; display: none; transition: 2s' role='alert'><span><strong>Ein Schwerwiegender Fehler ist aufgetreten :(<br>Bitte senden Sie einen Fehlerreport!</strong></span></div>
                        <div class='alert alert-danger' id="error-empty" style='margin: 20px; display: none; transition: 2s' role='alert'><span><strong>Bitte alle Felder ausfüllen!</strong></span></div>
                    </div>
                    <input class="form-control" type="text" id="input-username" name="input-username" placeholder="Benutzername" minlength="4" maxlength="20">
                    <input class="form-control" type="password" id="input-password" name="input-password" placeholder="Passwort" minlength="4" maxlength="20" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}">
                    <button class="btn btn-primary" id="button-login" name="button-login" onclick="handleLogin()">Login</button>
                    <p style="margin-top: 20px">Du hast noch kein Konto? <a href="register.php">Registrieren</a></p>
                </div>
            </div>
        </form>
    </div>
    <footer class="bg-white sticky-footer">
        <div class="container my-auto">
            <div class="text-center my-auto copyright"><span><?php echo $panelinfo?> | Copyright © ImNotYourDev 2019-2020</span></div>
        </div>
    </footer>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/ajax.js"></script>
</body>
</html>

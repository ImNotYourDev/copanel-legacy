<?php
/**
 * required for some needed functions
 */
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/functions.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/TaskManager.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/php/task/Task.php";

/**
 * check for login
 */
functions::isLoggedIn();
/**
 * check for new users
 */
if(functions::isNewUser()){
    functions::newLocation("changepassword.php");
}

$username = functions::getCurrentUserName();
$new_task_count = functions::getTaskManager()->countTasks(Task::TASK_USER, Task::TASK_TYPE_NEW);
$seen_task_count = functions::getTaskManager()->countTasks(Task::TASK_USER, Task::TASK_TYPE_PENDING);
$done_task_count = functions::getTaskManager()->countTasks(Task::TASK_USER, Task::TASK_TYPE_DONE);
$submitted_task_count = functions::getTaskManager()->countTasks(Task::TASK_USER, Task::TASK_TYPE_ALL_SUBMITTED);
$done_tasks = $done_task_count + $submitted_task_count;
$task_count = functions::getTaskManager()->countTasks(Task::TASK_USER, Task::TASK_TYPE_ALL);
$online_users = functions::countOnlineUsers();
$total_users = functions::countAllUsers();

$teacher = functions::isTeacher();
$panelinfo = functions::getPanelInfo();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <?php
    if($teacher){
        echo "<title>Lehrer CP | Dashboard</title>";
    }else{
        echo "<title>Schüler CP | Dashboard</title>";
    }
    ?>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion p-0" style="background-color: #212529; transition: 1s;">
            <div class="container-fluid d-flex flex-column p-0">
                <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="dashboard.php">
                    <div class="sidebar-brand-icon"><i class="far fa-user"></i></div>
                    <?php
                    if($teacher){
                        echo "<div class='sidebar-brand-text mx-3'><span>Lehrer CP</span></div>";
                    }else{
                        echo "<div class='sidebar-brand-text mx-3'><span>Schüler CP</span></div>";
                    }
                    ?>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="nav navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" href="dashboard.php"><i class="fas fa-tachometer-alt"></i><span>Dashboard</span></a>
                        <a class="nav-link" href="tasks.php" style="color: rgba(255,255,255,0.8);"><i class="fas fa-tasks"></i><span>Aufgaben</span></a>
                        <a class="nav-link" href="representationPlan.php"><i class="fas fa-table"></i><span>Vertretungsplan</span></a>
                        <a class='nav-link' href='exams.php'><i class='fas fa-sticky-note'></i><span>Prüfungen</span></a>
                        <a class='nav-link' href='group.php'><i class='fas fa-users'></i><span>Gruppe</span></a>
                        <?php
                        if($teacher){
                            echo "<hr />";
                            echo "<a class='nav-link' href='inbox.php'><i class='fas fa-newspaper'></i><span>Postfach</span></a>";
                            echo "<a class='nav-link' href='createTask.php'><i class='fas fa-plus'></i><span>Aufgaben erteilen</span></a>";
                            echo "<a class='nav-link' href='createdTasks.php'><i class='fas fa-list'></i><span>Erteilte Aufgaben</span></a>";
                            echo "<a class='nav-link' href='users.php'><i class='fas fa-users'></i><span>Benutzer</span></a>";
                            echo "<a class='nav-link' href='registerNewUser.php'><i class='fas fa-user-plus'></i><span>Benutzer hinzufügen</span></a>";
                            echo "<a class='nav-link' href='createGroup.php'><i class='fas fa-user-plus'></i><span>Gruppe hinzufügen</span></a>";
                        }
                        ?>
                        <hr />
                        <a class='nav-link' href='https://gitlab.com/ImNotYourDev/copanel-issue-tracker/-/issues'><i class='fas fa-bug'></i><span>Fehler melden</span></a>
                        <form method="post" action="php/post.php">
                            <button class="nav-link" name="button-logout" id="button-logout" type="submit" style="border: none; background-color: #232527"><i class='fas fa-sign-out-alt'></i><span>Ausloggen</span></button>
                        </form>
                    </li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <h3 class="text-dark mb-0">Dashboard</h3>
                    </div>
                </nav>
                <div class="alert alert-danger" style="margin-left: 20px; margin-right: 20px; height: 100px; text-align: center" role="alert"><span style="margin: 0; position: absolute; top: 50%; left: 50%; -ms-transform: translate(-50%, -50%); transform: translate(-50%, -50%)"><strong>BROADCAST: Panel ab morgen nicht mehr benutzbar!<br><a href="https://gitlab.com/codevs1/copanel">View legacy repository of COPanel</a></strong></span></div>
                <div class="alert alert-success" style="margin-left: 20px; margin-right: 20px" role="alert"><span><strong>Willkommen zurück <?php echo $username?>!</strong></span></div>
                <div class="card shadow border-left-danger py-2" style="margin-left: 10px; margin-right: 10px">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div style="float: left">
                                    <div class="text-uppercase text-danger font-weight-bold text-xs mb-1"><span>Neue Aufgaben</span></div>
                                    <div class="text-dark font-weight-bold h5 mb-0"><span id="new-task-count"><?php echo $new_task_count?></span></div>
                                </div>
                                <div style="float: left; margin-left: 20px">
                                    <button class="btn btn-primary" id="button-seen-dashboard" onclick="markAllAsread('dashboard')" style="display: none"><i class="fas fa-check-circle"></i> Alle als gesehen markieren</button>
                                    <?php
                                    if($new_task_count > 0){
                                        echo "<script>document.getElementById('button-seen-dashboard').style.display = null</script>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-auto"><i class="fas fa-tasks fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="card shadow border-left-warning py-2" style="margin-left: 10px; margin-right: 10px">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div class="text-uppercase text-warning font-weight-bold text-xs mb-1"><span>Wartende/Gesehene Aufgaben</span></div>
                                <div class="text-dark font-weight-bold h5 mb-0"><span id="seen-task-count"><?php echo $seen_task_count?></span></div>
                            </div>
                            <div class="col-auto"><i class="fas fa-eye fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="card shadow border-left-success py-2" style="margin-left: 10px; margin-right: 10px">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div class="text-uppercase text-success font-weight-bold text-xs mb-1"><span>Fertige Aufgaben</span></div>
                                <div class="text-dark font-weight-bold h5 mb-0"><span id="done-task-count"><?php echo $done_tasks?></span></div>
                            </div>
                            <div class="col-auto"><i class="fas fa-check-circle fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="card shadow border-left-warning py-2" style="margin-left: 10px; margin-right: 10px">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div class="text-uppercase text-success font-weight-bold text-xs mb-1"><span>Abgegebene Aufgaben</span></div>
                                <div class="text-dark font-weight-bold h5 mb-0"><span id="submitted-task-count"><?php echo $submitted_task_count?></span></div>
                            </div>
                            <div class="col-auto"><i class="fas fa-check-circle fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="card shadow border-left-info py-2" style="margin-left: 10px; margin-right: 10px">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div class="text-uppercase text-info font-weight-bold text-xs mb-1"><span>Alle Aufgaben(gesamt)</span></div>
                                <div class="text-dark font-weight-bold h5 mb-0"><span id="all-task-count"><?php echo $task_count?></span></div>
                            </div>
                            <div class="col-auto"><i class="fas fa-list fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="card shadow border-left-success py-2" style="margin-left: 10px; margin-right: 10px">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div class="text-uppercase text-success font-weight-bold text-xs mb-1"><span>Benutzer Online</span></div>
                                <div class="text-dark font-weight-bold h5 mb-0"><span><?php echo $online_users?>/<?php echo $total_users?></span></div>
                            </div>
                            <div class="col-auto"><i class="fas fa-user fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="d-sm-flex justify-content-between align-items-center mb-4"></div>
                </div>
                <div class="container"></div>
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span><?php echo $panelinfo?> | Copyright © ImNotYourDev 2019-2020</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
    <script src="assets/js/theme.js"></script>
    <script type="text/javascript" src="assets/js/ajax.js"></script>
</body>

</html>